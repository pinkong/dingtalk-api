/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.model.message;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/***
 * 图片消息
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 10:34
 */
public class DingMessageImage extends DingMessage {

    private final String mediaId;

    /**
     * 图片消息
     * @param mediaId 媒体文件id，可以通过媒体文件接口上传图片获取。建议宽600像素 x 400像素，宽高比3 : 2
     */
    public DingMessageImage(String mediaId) {
        super(MESSAGE_IMAGE);
        this.mediaId = mediaId;
    }

    @Override
    public String toString() {
        //得到文本消息类JSON字符串
        Map<String,Object> map=getDefaultMap();
        Map<String,Object> mediaId=new HashMap<>();
        mediaId.put("media_id",mediaId);
        map.put("image",mediaId);
        return JSON.toJSONString(map);
    }
}
