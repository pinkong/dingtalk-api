/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.model.message;

import java.util.HashMap;
import java.util.Map;

/***
 * 钉钉消息类型父类
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 9:56
 */
public abstract class DingMessage {
    //文本消息
    protected static final String MESSAGE_TEXT="text";
    //图片消息
    protected static final String MESSAGE_IMAGE="image";
    //语音消息
    protected static final String MESSAGE_VOICE="voice";
    //文件
    protected static final String MESSAGE_FILE="file";
    //链接消息
    protected static final String MESSAGE_LINK="link";
    //markdown消息
    protected static final String MESSAGE_MARKDOWN="markdown";
    //卡片消息
    protected static final String MESSAGE_CARD="action_card";


    /**
     * 消息类型
     */
    private final String type;

    public DingMessage(String type) {
        this.type = type;
    }

    protected Map<String,Object> getDefaultMap(){
        Map<String,Object> map=new HashMap<>();
        map.put("msgtype",type);
        return map;
    }
}
